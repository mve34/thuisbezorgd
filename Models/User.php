<?php

class User extends Model
{

    protected $table = 'users';

    /**
     * @Type varchar(255)
     */
    protected $firstname;

    /**
     * @Type varchar(255)
     */
    protected $lastname;

    /**
     * @Type varchar(255)
     */
    protected $email;

    /**
     * @Type varchar(255)
     */
    protected $password;

    /**
     * @Type varchar(255)
     */
    protected $city;

    /**
     * @Type varchar(255)
     */
    protected $postal_code;

    /**
     * @Type varchar(255)
     */
    protected $street;

    /**
     * @Type int(11)
     */
    protected $street_number;

    /**
     * @Type varchar(255)
     */
    protected $street_suffix;

    /**
     * @Type varchar(40)
     */
    protected $role;

    /**
     * @@Type boolean
     */
    protected $active = true;


    public function __construct()
    {

    }


    public function getFullName()
    {
        return $this->firstname . " " . $this->lastname;
    }


    private function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT);
    }


    private function checkPassword($password)
    {
        return password_verify($password, $this->password);
    }


    public static function generateSalt()
    {
        return uniqid();
    }


    protected static function newModel($obj)
    {

        $email = $obj->email;

        $existing = User::findBy('email', $email);
        if(count($existing) > 0) return false;

        //Check if user is valid
        return true;

    }


    public static function register($form)
    {
        if($form['password'] !== $form['repeat']) App::addError("passwords do not match");
        if(strlen($form['password']) < 8) App::addError("password is too short");

        if(isset($_SESSION['errors']) && count($_SESSION['errors'])) {
            return false;
        }

        $user = new User();
        $user->firstname = $form['firstname'];
        $user->lastname = $form['lastname'];
        $user->email = $form['email'];
        $user->setPassword($form['password']);
        $user->city = $form['city'];
        $user->postal_code = $form['postal_code'];
        $user->street = $form['street'];
        $user->street_number = $form['street_number'];
        $user->street_suffix = $form['street_suffix'];
        $user->role = 'user';
        $user->save();
        if($user->getId()) {
            App::setLoggedInUser($user);
            return $user;
        } else {
            return false;
        }
    }





    public static function login($form)
    {
        $email = $form['email'];
        $password = $form['password'];
        $users = self::findBy('email', $email);
        if (count($users) > 0) {
            $user = $users[0];
            if ($user->checkPassword($password)) {
                App::setLoggedInUser($user);
                return $user;
            }
        }
        App::addError("Combination does not exist");
        return false;
    }


    public static function updateUser($form)
    {
        $user = self::findById(App::$user->id);
        $user->firstname = $form['firstname'];
        $user->lastname = $form['lastname'];
        $user->email = $form['email'];
        $user->city = $form['city'];
        $user->postal_code = $form['postal_code'];
        $user->street = $form['street'];
        $user->street_number = $form['street_number'];
        $user->street_suffix = $form['street_suffix'];
        $user->save();
    }


    public static function loginForm()
    {
        $form = new Form();

        $form->addField((new FormField("email"))
            ->type("email")
            ->placeholder("E-mail")
            ->required());

        $form->addField((new FormField("password"))
            ->type("password")
            ->placeholder("Password")
            ->required());

        return $form->getHTML();
    }


    public static function registerForm()
    {
        $form = new Form();

        $form->addField((new FormField("firstname"))
            ->placeholder("First name")
            ->required());

        $form->addField((new FormField("lastname"))
            ->placeholder("Last name")
            ->required());

        $form->addField((new FormField("email"))
            ->type("email")
            ->placeholder("E-mail")
            ->required());

        $form->addField((new FormField("password"))
            ->type("password")
            ->placeholder("Password")
            ->required());

        $form->addField((new FormField("repeat"))
            ->type("password")
            ->placeholder("Password repeat")
            ->required());

        $form->addField((new FormField("city"))
            ->placeholder("city")
            ->required());

        $form->addField((new FormField("postal_code"))
            ->placeholder("postal code")
            ->required());

        $form->addField((new FormField("street"))
            ->placeholder("street")
            ->required());

        $form->addField((new FormField("street_number"))
            ->type("number")
            ->placeholder("street number")
            ->required());

        $form->addField((new FormField("street_suffix"))
            ->placeholder("street suffix"));

        return $form->getHTML();
    }


    public static function editUserForm()
    {
        $user = User::findById(App::$user->id);

        $form = new Form();

        $form->addField((new FormField("firstname"))
            ->placeholder("First name")
            ->value($user->firstname)
            ->required());

        $form->addField((new FormField("lastname"))
            ->placeholder("Last name")
            ->value($user->lastname)
            ->required());

        $form->addField((new FormField("email"))
            ->type("email")
            ->placeholder("E-mail")
            ->value($user->email)
            ->required());

        $form->addField((new FormField("city"))
            ->placeholder("city")
            ->value($user->city)
            ->required());

        $form->addField((new FormField("postal_code"))
            ->placeholder("postal code")
            ->value($user->postal_code)
            ->required());

        $form->addField((new FormField("street"))
            ->placeholder("street")
            ->value($user->street)
            ->required());

        $form->addField((new FormField("street_number"))
            ->placeholder("streetnumber")
            ->value($user->street_number)
            ->required());

        $form->addField((new FormField("street_suffix"))
            ->placeholder("streetsuffix")
            ->value($user->street_suffix)
            ->required());

        return $form->getHTML();
    }

}
