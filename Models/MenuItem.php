<?php

use Intervention\Image\ImageManagerStatic as Image;

class MenuItem extends Model
{

    protected $table = 'menu_items';

    /**
     * @Type int
     */
    protected $restaurant_id;

    /**
     * @Type varchar(255)
     */
    protected $name;

    /**
     * @Type varchar(255)
     */
    protected $image;

    /**
     * @Type varchar(500)
     */
    protected $description;

    /**
     * @Type int(11)
     */
    protected $type;

    /**
     * @Type double(9,2)
     */
    protected $price;




    public function getName()
    {
        return $this->name;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getPrice()
    {
        return $this->price;
    }



    protected static function newModel($obj)
    {
        return true;
    }


    public static function addMenu($form)
    {
        if (isset($_SESSION['errors']) && count($_SESSION['errors'])) {
            return false;
        }
        $menuItem = new MenuItem();
        $menuItem->name = $form['name'];
        if ( !!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);

            if($menuItem->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image);
            }

            $menuItem->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];

            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png', 'PNG'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image)) {
                    // the file has been moved correctly, now resize it

                    // open and resize an image file
                    $img = Image::make(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image)->resize(300, 200);

                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image, 100);

                    // open an image file
                    $img = Image::make(Http::$dirroot.'public/images/'.$menuItem->image);

                    // now you are able to resize the instance
                    $img->resize(320, 240);

                    // finally we save the image as a new file
                    $img->save(Http::$dirroot.'public/images/'.$menuItem->image,100);
                }
            }
            else {
                $menuItem->image = "";
            }
        }

        $menuItem->description = $form['description'];
        $menuItem->type = $form['type'];
        $menuItem->price = $form['price'];
        $menuItem->restaurant_id = $_GET['restaurant_id'];
        $menuItem->save();
    }

    public static function editMenu($form)
    {
        $menuItem = new MenuItem();

        $menuItem->name = $form['name'];
        if (!!$_FILES['logo']['tmp_name']) {
            $fileParts = pathinfo($_FILES['logo']['name']);

            if($menuItem->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image);
            }

            $menuItem->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];

            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png', 'PNG'])) {
                if(move_uploaded_file($_FILES['logo']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image)) {
                    // the file has been moved correctly, now resize it

                    // open and resize an image file
                    $img = Image::make(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image)->resize(300, 200);

                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menuItem->image, 100);

                    // open an image file
                    $img = Image::make(Http::$dirroot.'public/images/'.$menuItem->image);

                    // now you are able to resize the instance
                    $img->resize(320, 240);

                    // finally we save the image as a new file
                    $img->save(Http::$dirroot.'public/images/'.$menuItem->image,100);
                }
            }
            else {
                $menuItem->image = "";
            }
        }

        $menuItem->description = $form['description'];
        $menuItem->type = $form['type'];
        $menuItem->price = $form['price'];
        $menuItem->restaurant_id = $_GET['restaurant_id'];
        $menuItem->save();
    }

    public static function menuForm()
    {
        $user = App::getUser();
        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("menu item name")
            ->required());

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("upload food image")
            ->required());

        $form->addField((new FormField("description"))
            ->placeholder("menu item description")
            ->required());

        $form->addField((new FormField("type"))
            ->placeholder("menu item type")
            ->type("select")
            ->value(["appetizer", "main course", "dessert", "drinks"])
            ->required());

        $form->addField((new FormField("price"))
            ->placeholder("item price")
            ->required());

        return $form->getHTML();
    }

    public static function editMenuForm()
    {
        $restaurant = Restaurant::findById($_GET['restaurant_id']);

        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("restaurant name")
            ->value($restaurant->name)
            ->required());

        $form->addField((new FormField("logo"))
            ->type("file")
            ->placeholder("Logo")
            ->value($restaurant->logo));

        $form->addField((new FormField("description"))
            ->placeholder("Description")
            ->value($restaurant->description)
            ->required());

        $form->addField((new FormField("city"))
            ->placeholder("city")
            ->value($restaurant->city)
            ->required());

        $form->addField((new FormField("postal_code"))
            ->placeholder("postal code")
            ->value($restaurant->postal_code)
            ->required());

        $form->addField((new FormField("street"))
            ->placeholder("street")
            ->value($restaurant->street)
            ->required());

        $form->addField((new FormField("street_number"))
            ->placeholder("streetnumber")
            ->value($restaurant->street_number)
            ->required());

        $form->addField((new FormField("street_suffix"))
            ->placeholder("streetsuffix")
            ->value($restaurant->street_suffix));

        $form->addField((new FormField("open_at"))
            ->type("time")
            ->placeholder("open from")
            ->value($restaurant->open_at)
            ->required());

        $form->addField((new FormField("closed_at"))
            ->type("time")
            ->placeholder("closed at")
            ->value($restaurant->closed_at)
            ->required());

        return $form->getHTML();
    }

}