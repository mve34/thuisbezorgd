<?php

use Intervention\Image\ImageManagerStatic as Image;

class Restaurant extends Model
{

    protected $table = 'restaurants';

    /**
     * @Type int
     */
    protected $user_id;

    /**
     * @Type varchar(255)
     */
    protected $name;

    /**
     * @Type varchar(255)
     */
    protected $description;

    /**
     * @Type varchar(255)
     */
    protected $logo;

    /**
     * @Type varchar(255)
     */
    protected $city;

    /**
     * @Type varchar(255)
     */
    protected $postal_code;

    /**
     * @Type varchar(255)
     */
    protected $street;

    /**
     * @Type int(11)
     */
    protected $street_number;

    /**
     * @Type varchar(255)
     */
    protected $street_suffix;

    /**
     * @Type varchar(8)
     */
    protected $open_at;

    /**
     * @Type varchar(8)
     */
    protected $closed_at;




    public function getName()
    {
        return $this->name;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getPostalCode()
    {
        return $this->postal_code;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getStreetNumber()
    {
        return $this->street_number;
    }

    public function getStreetSuffix()
    {
        return $this->street_suffix;
    }



    protected static function newModel($obj)
    {

        $email = $obj->email;

        $existing = Restaurant::findBy('email', $email);
        if(count($existing) > 0) return false;

        //Check if user is valid
        return true;

    }


    public static function newRestaurant($form)
    {
        if (isset($_SESSION['errors']) && count($_SESSION['errors'])) {
            return false;
        }
        $restaurant = new Restaurant();
        $restaurant->name = $form['name'];
        if (!!$_FILES['logo']['tmp_name']) {
            $fileParts = pathinfo($_FILES['logo']['name']);
            if ($restaurant->logo) {
                @unlink(Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo);
            }
            $restaurant->logo = sha1($fileParts['filename'] . microtime()) . '.' . $fileParts['extension'];
            if (in_array($fileParts['extension'], ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG'])) {
                if (move_uploaded_file($_FILES['logo']['tmp_name'], Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo)) {
                    // the file has been moved correctly, now resize it
                    // open and resize an image file
                    $img = Image::make(Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo)->resize(300, 200);

                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo, 100);

                    // open an image file
                    $img = Image::make(Http::$dirroot . 'public/images/' . $restaurant->logo);

                    // now you are able to resize the instance
                    $img->resize(320, 240);

                    // finally we save the image as a new file
                    $img->save(Http::$dirroot . 'public/images/' . $restaurant->logo, 100);
                }
            } else {
                $restaurant->logo = "";
            }
        }
        $restaurant->description = $form['description'];
        $restaurant->postal_code = $form['postal_code'];
        $restaurant->city = $form['city'];
        $restaurant->street = $form['street'];
        $restaurant->street_number = $form['street_number'];
        $restaurant->street_suffix = $form['street_suffix'];
        $restaurant->open_at = $form['open_at'];
        $restaurant->closed_at = $form['closed_at'];
        $restaurant->user_id = App::$user->id;
        $restaurant->save();
    }

    public static function updateRestaurant($form)
    {
        $restaurant = Restaurant::findById($_GET['restaurant_id']);
        $restaurant->name = $form['name'];
        if (!!$_FILES['logo']['tmp_name']) {
            $fileParts = pathinfo($_FILES['logo']['name']);
            if ($restaurant->logo) {
                @unlink(Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo);
            }
            $restaurant->logo = sha1($fileParts['filename'] . microtime()) . '.' . $fileParts['extension'];
            if (in_array($fileParts['extension'], ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG'])) {
                if (move_uploaded_file($_FILES['logo']['tmp_name'], Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo)) {
                    // the file has been moved correctly, now resize it
                    // open and resize an image file
                    $img = Image::make(Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo)->resize(300, 200);

                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $restaurant->logo, 100);

                    // open an image file
                    $img = Image::make(Http::$dirroot . 'public/images/' . $restaurant->logo);

                    // now you are able to resize the instance
                    $img->resize(320, 240);

                    // finally we save the image as a new file
                    $img->save(Http::$dirroot . 'public/images/' . $restaurant->logo, 100);
                }
            } else {
                $restaurant->logo = "";
            }
        }
        $restaurant->description = $form['description'];
        $restaurant->postal_code = $form['postal_code'];
        $restaurant->city = $form['city'];
        $restaurant->street = $form['street'];
        $restaurant->street_number = $form['street_number'];
        $restaurant->street_suffix = $form['street_suffix'];
        $restaurant->open_at = $form['open_at'];
        $restaurant->closed_at = $form['closed_at'];
        $restaurant->save();
    }

    public static function restaurantForm()
    {
        $user = App::getUser();
        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("restaurant name")
            ->required());

        $form->addField((new FormField("logo"))
            ->type("file")
            ->placeholder("upload restaurant logo")
            ->required());

        $form->addField((new FormField("description"))
            ->placeholder("restaurant description")
            ->required());

        $form->addField((new FormField("postal_code"))
            ->placeholder("postal code")
            ->required());

        $form->addField((new FormField("city"))
            ->placeholder("city")
            ->required());

        $form->addField((new FormField("street"))
            ->placeholder("street name")
            ->required());

        $form->addField((new FormField("street_number"))
            ->type("number")
            ->placeholder("street number")
            ->required());

        $form->addField((new FormField("street_suffix"))
            ->placeholder("street suffix"));

        $form->addField((new FormField("open_at"))
            ->type("time")
            ->placeholder("open from")
            ->value("00:00")
            ->required());

        $form->addField((new FormField("closed_at"))
            ->type("time")
            ->placeholder("closed at")
            ->value("00:00")
            ->required());


        return $form->getHTML();
    }

    public static function editUserForm()
    {
        $restaurant = Restaurant::findById($_GET['restaurant_id']);

        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("restaurant name")
            ->value($restaurant->name)
            ->required());

        $form->addField((new FormField("logo"))
            ->type("file")
            ->placeholder("Logo")
            ->value($restaurant->logo));

        $form->addField((new FormField("description"))
            ->placeholder("Description")
            ->value($restaurant->description)
            ->required());

        $form->addField((new FormField("city"))
            ->placeholder("city")
            ->value($restaurant->city)
            ->required());

        $form->addField((new FormField("postal_code"))
            ->placeholder("postal code")
            ->value($restaurant->postal_code)
            ->required());

        $form->addField((new FormField("street"))
            ->placeholder("street")
            ->value($restaurant->street)
            ->required());

        $form->addField((new FormField("street_number"))
            ->placeholder("streetnumber")
            ->value($restaurant->street_number)
            ->required());

        $form->addField((new FormField("street_suffix"))
            ->placeholder("streetsuffix")
            ->value($restaurant->street_suffix));

        $form->addField((new FormField("open_at"))
            ->type("time")
            ->placeholder("open from")
            ->value($restaurant->open_at)
            ->required());

        $form->addField((new FormField("closed_at"))
            ->type("time")
            ->placeholder("closed at")
            ->value($restaurant->closed_at)
            ->required());

        return $form->getHTML();
    }

}