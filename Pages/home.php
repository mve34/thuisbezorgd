<?php

//$restaurants = Restaurant::get();

if(isset($_GET['search'])){
    $restaurantName = DB::getInstance()->prepare('
        SELECT *
        FROM restaurants 
        WHERE name LIKE :name');
    $restaurantName->execute(['name' => '%' . $_GET['search']. '%']);
    $restaurants = $restaurantName->fetchAll(PDO::FETCH_CLASS, 'Restaurant');
}
else{
    $restaurants = Restaurant::get();
}

?>

<div class="container">
    <div class="blockContainer">
        <div class="block w8">
            <h1>
                welcome to Home Delivered
            </h1>
            <form method="get" action="http://localhost/thuisbezorgd/public/">
                <input type="hidden" name="page" value="<?= $_GET['page']; ?>" />
                <input type="text" name="search" />
                <button type="submit">Zoek</button>
            </form>
            <table>

                <?php
                foreach($restaurants as $restaurant) {?>
                    <tr>
                        <td>
                            <img src="<?= Http::$webroot.'images/'.$restaurant->logo; ?>"><br/>
                        </td>
                        <td>
                            <h4>
                                <a <?= App::link('restaurant&id='.$restaurant->id) ?> >
                                <?= $restaurant->name;?>
                                </a><br/>
                            </h4>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>