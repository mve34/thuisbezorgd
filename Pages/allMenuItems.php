<?php

$restaurant = $_GET['restaurant_id'];
$menuItems = MenuItem::findBy("restaurant_id", $restaurant);

if(Restaurant::findById($_GET['restaurant_id'])->user_id != App::$user->id){
    die("you didn't make this");
}

?>
<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Edit account information
        </div>
        <div class="card-body">
            <?php
            foreach ($menuItems as $menuItem){ ?>

                <a <?php echo App::link('editMenu&restaurant_id=' . $restaurant);?>>
                    <?php echo $menuItem->name . "<br/>"; ?>
                </a>
            <?php } ?>
            <a <?= App::link("addMenuItem&restaurant_id=" . $_GET['restaurant_id']) ?>>add menu item</a>
        </div>
    </div>
</div>