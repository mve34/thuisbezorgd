<?php

App::pageAuth([App::ROLE_USER], 'login');
$menus = MenuItem::findBy('restaurant_id', $_GET['id']);

?>
<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Register
        </div>
        <div class="card-body">
            <?php foreach ($menus as $menu){?>
                <img class="float-left" src="images/<?= $menu->image ?>">
                <h1 class=""><?= $menu->name ?></h1>
                <a class="float-right" <?= App::link('cart&id=' . $_GET['id']); ?>>Add to cart</a>
            <?php } ?>
        </div>
    </div>
</div>
