<?php

App::pageAuth([App::ROLE_USER]);

if(isset($_POST['name'])) {
    MenuItem::addMenu($_POST);
}

if(Restaurant::findById($_GET['restaurant_id'])->user_id != App::$user->id){
    die("you didn't make this");
}

?>
<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Register
        </div>
        <div class="card-body">
            <?= MenuItem::menuForm() ?>
        </div>
    </div>
</div>