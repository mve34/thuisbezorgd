<?php

App::pageAuth([App::ROLE_USER]);

if(isset($_POST['name'])) {
    MenuItem::editMenu($_POST);
}

?>
<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Register
        </div>
        <div class="card-body">
            <?= MenuItem::editMenuForm(); ?>
        </div>
    </div>
</div>