<?php

$restaurant = Restaurant::findById($_GET['restaurant_id']);

if(Restaurant::findById($_GET['restaurant_id'])->user_id != App::$user->id){
    die("you didn't make this");
}

?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            <?= $restaurant->name ?>
        </div>
        <div class="card-body">
            restaurant info: <a type="button" id="editRestaurant" <?= App::link("editRestaurant&restaurant_id=" . $restaurant->id) ?>><?= $restaurant->name ?></a><br/>
            Menu items: <a type="button" id="editMenu" <?= App::link("allMenuItems&restaurant_id=" . $restaurant->id) ?>><?= $restaurant->name ?></a>
        </div>
    </div>
</div>

