<?php

$user = App::getUser();
$restaurants = Restaurant::findBy("user_id", $user->id);


?>
<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Edit account information
        </div>
        <div class="card-body">
            <?php
            foreach ($restaurants as $restaurant){ ?>

                <a <?php echo App::link('selectAction&restaurant_id=' . $restaurant->id);?>>
                    <?php echo $restaurant->name . "<br/>"; ?>
                </a>
            <?php } ?>
            <a <?= App::link("addRestaurant") ?>>add restaurant</a>
        </div>
    </div>
</div>
