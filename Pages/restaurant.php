<?php

$restaurant = Restaurant::findById($_GET['id']);

?>

<div class="container">

    <div class="blockContainer">

        <div class="block w16">
            <div class="right">
                <img src="<?= Http::$webroot.'images/'.$restaurant->logo;?>">
            </div>
            <h1>
                <?= $restaurant->name ?>
            </h1>
            <h4>
                <div class="left">
                    <p>
                        <?= $restaurant->description ?><br/><br/>

                        <b>This restaurant is located in:</b> <br/>
                        <?= $restaurant->city ?><br/>
                        <?= $restaurant->street . " " . $restaurant->street_number . " "  . $restaurant->street_suffix ?>
                    </p>
                    <a type="button" <?= App::link('menuView&id=' . $_GET['id']); ?>>Order food</a>
                </div>

            </h4>
        </div>
    </div>
</div>


